OCM Data Snapshots convert to CSV
---------------
**Deprecated: data exports will be moving to https://github.com/openchargemap/ocm-export which provides a file per-POI resulting in much more efficient changes/cloning**
----------------------------------------

- To run


    - `yarn install` 
    - `node --stack-size=32000 main.js`

- The repository may have a large history changes, so clone with: `git clone https://github.com/openchargemap/ocm-data --depth 1` to get the fastest/smallest download.