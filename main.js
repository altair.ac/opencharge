const path = require('path');
const fs = require('fs');
const ObjectsToCsv = require('objects-to-csv');

const files = [];

function traverseDir(dir) {
    fs.readdirSync(dir).forEach(file => {
        let fullPath = path.join(dir, file);
        if (fs.lstatSync(fullPath).isDirectory()) {
            traverseDir(fullPath);
        } else {
            if (fullPath.split(".")[1] === 'json') {
                if (!fullPath.includes("referencedata")) {
                    files.push(fullPath);
                }
            }
        }
    });
}

async function getParsedObject(file_path) {
    try {
        return JSON.parse(fs.readFileSync(file_path));
    } catch (error) {
        console.log(error);
        console.log(file_path);
    }
}

async function getUsageTypeInfo(id) {
    const reference = JSON.parse(fs.readFileSync("data/referencedata.json")).UsageTypes;
    return reference.find((x) => x.ID === id);
}

// (usageType.IsAccessKeyRequired === null) ? ("Not Available") : ((usageType.IsAccessKeyRequired) ? ("Required") : ("Not Required"))

async function sanitizeObject(object) {
    const rows = object.Connections.length;
    const return_rows = [];
    let usageType;
    if (object.UsageTypeID) {
        usageType = await getUsageTypeInfo(object.UsageTypeID);
    } else {
        usageType = await getUsageTypeInfo(0);
    }
    for (let index = 0; index < rows; index++) {
        const element = object.Connections[index];
        return_rows.push({
            UUID: object.UUID,
            DataProviderID: object.DataProviderID,
            OperatorID: object.OperatorID,
            UsageTypeID: object.UsageTypeID,
            Usage_IsPayAtLocation: (usageType.IsPayAtLocation === null) ? ("Not Available") : ((usageType.IsPayAtLocation) ? ("Required") : ("Not Required")),
            Usage_IsMembershipRequired: (usageType.IsMembershipRequired === null) ? ("Not Available") : ((usageType.IsMembershipRequired) ? ("Required") : ("Not Required")),
            Usage_IsAccessKeyRequired: (usageType.IsAccessKeyRequired === null) ? ("Not Available") : ((usageType.IsAccessKeyRequired) ? ("Required") : ("Not Required")),
            Usage_ID: usageType.ID,
            Usage_Title: usageType.Title,
            UsageCost: object.UsageCost,
            AddressInfo_Title: object.AddressInfo.Title,
            AddressInfo_Town: object.AddressInfo.Town,
            AddressInfo_StateOrProvince: object.AddressInfo.StateOrProvince,
            AddressInfo_CountryID: object.AddressInfo.CountryID,
            AddressInfo_Latitude: object.AddressInfo.Latitude,
            AddressInfo_Longitude: object.AddressInfo.Longitude,
            AddressInfo_Postcode: object.AddressInfo.Postcode,
            Connections_Amps: element.Amps,
            Connections_LevelID: element.LevelID,
            Connections_PowerKW: element.PowerKW,
            Connections_Voltage: element.Voltage,
            NumberOfPoints: object.NumberOfPoints
        });
    }
    return return_rows;
}

async function toDisk(sanitized_object) {
    const csv = new ObjectsToCsv(sanitized_object)
    await csv.toDisk('./list.csv');
}

async function main() {
    // get all file paths into memory
    await traverseDir("data");

    let objects_to_csv = [];
    for (let index = 0; index < files.length; index++) {
        const element = files[index];
        const object = await getParsedObject(element);
        const sanitized_object = await sanitizeObject(object);
        objects_to_csv = objects_to_csv.concat(sanitized_object);
        console.log(index);
    }
    await toDisk(objects_to_csv);

}


(async () => {
    try {
        await main();
    } catch (error) {
        console.log(error);
    }
    process.exit(0);
})();